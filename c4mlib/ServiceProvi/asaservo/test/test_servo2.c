/**
 * @file test_servo.c
 * @author 葉子哇 (w8indow61231111@gmail.com)
 * @date 2020.11.19
 * @brief
 * AsaServo_PwmPrePro_cmd 測試程式，測試「速度指令」遞增時實際上轉速是否連續
 */

#include "c4mlib/ServiceProvi/asaservo/src/servo.h"

int main() {
    C4M_DEVICE_set();
    printf("Start\n");
    ASASERVO_PWMPREPRO_LAY();
    PwmTable_Print(&PWM_str);
    _delay_ms(100);
    sei();
    while (1) {
        for (int8_t count = -57; count <= 57; count++) {
            AsaServo_PwmPrePro_cmd(&PWM_str, 1, (int8_t)count);
            AsaServo_PwmPrePro_cmd(&PWM_str, 2, -(int8_t)count);
            _delay_ms(200);
        }
        for (int8_t count = 57; count >= -57; count--) {
            AsaServo_PwmPrePro_cmd(&PWM_str, 1, (int8_t)count);
            AsaServo_PwmPrePro_cmd(&PWM_str, 2, -(int8_t)count);
            _delay_ms(200);
        }
    }
}
