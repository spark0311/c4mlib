/**
 * @file test_servo.c
 * @author 葉子哇 (w8indow61231111@gmail.com)
 * @date 2020.11.19
 * @brief AsaServo_PwmPrePro_cmd 測試程式，給速度指令輸出該速度所對應之PWM
 * 
 */

#include "c4mlib/ServiceProvi/asaservo/src/servo.h"

int main() {
    C4M_DEVICE_set();
    printf("Start\n");
    ASASERVO_PWMPREPRO_LAY();
    PwmTable_Print(&PWM_str);
    _delay_ms(100);
    sei();
    while (1) {
        int speed;
        printf("Enter Speed\n");
        scanf("%d", &speed);
        printf("%d\n",speed);
        AsaServo_PwmPrePro_cmd(&PWM_str, 1, (int8_t)speed);
        AsaServo_PwmPrePro_cmd(&PWM_str, 2, -(int8_t)speed);
        _delay_ms(100);
    }
}
