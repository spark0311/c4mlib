/**
 * @file test_flash_w25q128jv_write.c
 * @author LCY (nacmvmclab@gmail.com)
 * @date 2020.10.07
 * @brief 使用SPI Master記憶傳輸和接收驅動函式測試w25q128jv flash IC
 * 蓋寫資料功能。
 *
 */

#include "c4mlib/C4MBios/asabus/src/asabus.h"
#include "c4mlib/C4MBios/asabus/src/pin_def.h"
#include "c4mlib/ServiceProvi/asaspi/src/asaspi_master.h"
#include "c4mlib/C4MBios/hardware/src/hal_spi.h"

#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/C4MBios/hardwareset/src/m128/spi_set.h"
#include "c4mlib/ServiceProvi/flash/test/spi_test.cfg"

#include "c4mlib/ServiceProvi/flash/src/flash.h"
#include "c4mlib/ServiceProvi/flash/src/w25q128jv.h"

#define SPI_MODE        6
#define ASAID           8

uint32_t mem_addr = 0;
uint8_t manu_cont[3] = {0, 0, 0};
uint8_t test_rec[4] = {0, 0, 0 ,0};
uint8_t test_id[2] = {0, 0};
uint8_t test_write[3] = {0x12, 0x12, 0x12};
uint8_t addr[3] = {0, 0, 0};

uint8_t SPI_init(void){
    HardWareSet_t SPIHWSet_str = SPIHWSETSTRINI;
    HWFlagPara_t SPIFgGpData_str[11] = SPIFLAGPARALISTINI;
    HWRegPara_t SPIRegData_str = SPIREGPARALISTINI;
    SPIHWSetDataStr_t SPIHWSetData = SPI_FG_DATA_INT;
    HARDWARESET_LAY(SPIHWSet_str, SPIFgGpData_str[0], SPIRegData_str, SPIHWSetData);
    return HardwareSet_step(&SPIHWSet_str);
}

int main() {
    // Setup
    uint64_t ADDR = 0x000010;

    C4M_STDIO_init();

    // FIXME: 待更新..
    DDRB |= 0x07;
    SPI_init();

    Extern_CS_disable(ASAID);
    printf("Start test flash Read & Write...\n");
    printf("=================\n");

    Extern_CS_enable(ASAID);
    ASABUS_SPI_swap(SPI_DUMMY);  
    Extern_CS_disable(ASAID);

    Flash_manufacture_code(manu_cont);

    for (uint8_t i = 0; i < 2; i++) {
        printf("manu_cont=>%x\n", manu_cont[i]);
    }

    Flash_clear_sector(SECTOR(BLOCK(0), 0));

    Flash_page_write(ADDR, 3, test_write);


    printf("--READ_4BYTES_DATA--\n");
    Flash_read_data(ADDR, 4, test_rec);
    
    printf("read--1 = %d\n", test_rec[0]);
    printf("read--2 = %d\n", test_rec[1]);
    printf("read--3 = %d\n", test_rec[2]);

    return 0;
}
