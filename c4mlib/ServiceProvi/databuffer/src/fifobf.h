/**
 * @file fifobf.h
 * @author YP-Shen , jerryhappyball
 * @date 2020.04.22
 * @brief
 * 實現先進先出緩衝區讀寫之功能，可提供使用者，前後級同時讀寫應用的資料緩衝。
 */

#ifndef C4MLIB_DATABUFFER_FIFOBUFFER_H
#define C4MLIB_DATABUFFER_FIFOBUFFER_H

#include <avr/interrupt.h>
#include <stdint.h>

/**
 * @defgroup databuffer_macro fifobuffer macros
 * @defgroup databuffer_struct fifobuffer structs
 * @defgroup databuffer_func fifobuffer functions
 */

/* Public Section Start */
/**
 * @brief   供管理結構體鏈結到前後級工作之緩衝區欄
 * @ingroup databuffer_macro
 */
#define NETFBF(task_p, num, buffer_p) task_p->FBF[num] = buffer_p

/**
 * @brief 供暫存資料之矩陣，鍊結至管理先進先出之結構體，以及初始化先進先出結構體
 * @param Str 定義批次緩衝暫存器建構
 * @param List[MAXNUM] 定義資料陣列用矩陣
 * @ingroup databuffer_macro
 */
#define FIFOBUFF_LAY(FBSTR, MAXNUM)                                            \
    static FifoBFStr_t FBSTR = {0};                                                   \
    FBSTR.Depth       = MAXNUM;                                                \
    {                                                                          \
        static uint8_t List[MAXNUM] = {0};                                     \
        FBSTR.List_p                = List;                                    \
    }

/**
 * @brief   定義先進先出結構體以管理先進先出緩衝區狀態之結構
 * @ingroup databuffer_struct
 */
typedef struct {
    uint8_t PIndex;  ///< FIFO Buffer 的寫入索引
    uint8_t GIndex;  ///< FIFO Buffer 的讀取索引
    uint8_t Total;   ///< FIFO Buffer 中目前已存入的資料數
    uint8_t Depth;   ///< FIFO Buffer 中可存入的資料入，即為大小
    void* List_p;    ///< FIFO Buffer 中的資料起始記憶體位置
} FifoBFStr_t;

/**
 * @ingroup databuffer_func
 *
 * @brief 資料寫入函式，供前級工作呼叫以存入新資料。
 *
 * @param *FBF_p     欲存入資料的批次緩衝區指標。
 * @param *Data_p    提供存入資料的變數或矩陣指標。
 * @param Bytes     資料位元組數。
 * @return
 * 尚餘緩衝空間容量位元組數n，若n大於等於0代表剩餘n位元組可存入，否則為不足n位元組。
 */
int8_t FifoBF_put(FifoBFStr_t* FBF_p, void* Data_p, uint8_t Bytes);

/**
 * @ingroup databuffer_func
 *
 * @brief 資料讀出函式，供前級工作呼叫以讀出新資料。
 *
 * @param *FBF_p     欲讀出資料的批次緩衝區指標。
 * @param *Data_p    提供讀出資料的變數或矩陣指標。
 * @param Bytes     資料位元組數
 * @return
 * 尚餘緩衝空間容量位元組數n，若n大於等於0代表剩餘n位元組可讀出，否則為不足n位元組。
 */
int8_t FifoBF_get(FifoBFStr_t* FBF_p, void* Data_p, uint8_t Bytes);

/**
 * @ingroup databuffer_func
 *
 * @brief 供前級工作呼叫以清空緩衝區。
 *
 * @param *FBF_p     欲清空批次緩衝區指標。
 * @return 緩衝區可存入之資料位元數
 */
uint8_t FifoBF_clr(FifoBFStr_t* FBF_p);
/* Public Section End */

#endif  // C4MLIB_DATABUFFER_FIFOBUFFER_H
