/**
 * @file test_bbf_float.c
 * @author jerryhappyball
 * @date 2020-03-17
 * @brief BatchBuffer float data test
 */

#include "c4mlib/ServiceProvi/databuffer/src/bbf.h"
#include "c4mlib/C4MBios/device/src/device.h"

int main() {
    C4M_DEVICE_set();

    BATCHBUFF_LAY(ABBF_str, 16);

    uint8_t FrontData[2] = {1, 3};
    float FrontData2[3]  = {-5.9, 7.8, 9.6};

    /// Write1:
    BatchBF_put(&ABBF_str, FrontData, 2);  //寫入第一次資料

    BatchBF_clr(&ABBF_str);  //清除已寫入資料

    /// Write2:
    BatchBF_put(&ABBF_str, FrontData2, sizeof(FrontData2));  //寫入第二次資料
    printf("Write:{");
    for (int i = 0; i < sizeof(FrontData2) / sizeof(float); i++) {
        printf("%g ", ((float*)ABBF_str.List_p)[i]);
    }
    printf("}\n");

    BatchBF_full(&ABBF_str);  //強制為滿

    /// Read1:
    float RearData[2] = {0}, RearData2[1] = {0};
    BatchBF_get(&ABBF_str, RearData, sizeof(RearData));  //讀出第一次資料
    printf("Read:{");
    for (int i = 0; i < 2; i++) {
        if (!i)
            printf("%g", RearData[i]);
        else
            printf(" %g", RearData[i]);
    }
    printf("}\n");

    /// Read2:
    printf(
        "BBF_p->State in BBF_get = %d\n",
        BatchBF_get(&ABBF_str, RearData2, sizeof(RearData2)));  //讀出第二次資料
    printf("Read:{");
    printf("%g", RearData2[0]);
    printf("}\n");
}
