/**
 * @file test_remobf_all.c
 * @author Yi-Mou
 * @brief 提供操作remobf全部變數的測試
 * @date 2020.03.24
 *
 */

#include "c4mlib/ServiceProvi/databuffer/src/remobf.h"
#include "c4mlib/C4MBios/device/src/device.h"

int main() {
    C4M_DEVICE_set();
    REMOBUFF_LAY(RemoBF_Str, 10, 3);

    uint8_t PID[3] = {0};
    uint32_t speed = 0;
    RemoBF_reg(&RemoBF_Str, PID, 3);
    RemoBF_reg(&RemoBF_Str, &speed, 4);

    /* 將遠端的變數依序填入登錄的變數緩衝區之中 */
    uint8_t PID1[3] = {3, 18, 8};
    uint32_t speed1 = 23156;

    PID1[0] = 128;
    PID1[1] = 128;
    PID1[2] = 128;
    speed1  = 37;

    RemoBF_temp(&RemoBF_Str, 255, PID1[0]);
    RemoBF_temp(&RemoBF_Str, 255, PID1[1]);
    RemoBF_temp(&RemoBF_Str, 255, PID1[2]);
    RemoBF_temp(&RemoBF_Str, 255, *(uint8_t*)(&speed1));
    RemoBF_temp(&RemoBF_Str, 255, *((uint8_t*)(&speed1) + 1));
    RemoBF_temp(&RemoBF_Str, 255, *((uint8_t*)(&speed1) + 2));
    RemoBF_temp(&RemoBF_Str, 255, *((uint8_t*)(&speed1) + 3));

    /* 將緩衝區中的內容移至登錄的變數位址之中 */
    RemoBF_put(&RemoBF_Str, 255);

    printf("%d , %d ,%d\n", PID[0], PID[1], PID[2]);
    printf("%ld\n", speed);

    /* 讀取變數位址中的內容 */
    uint8_t PID2[3] = {0};
    uint32_t speed2 = 0;

    RemoBF_get(&RemoBF_Str, 255, &PID2[0]);
    RemoBF_get(&RemoBF_Str, 255, &PID2[1]);
    RemoBF_get(&RemoBF_Str, 255, &PID2[2]);
    RemoBF_get(&RemoBF_Str, 255, ((uint8_t*)&speed2));
    RemoBF_get(&RemoBF_Str, 255, ((uint8_t*)&speed2) + 1);
    RemoBF_get(&RemoBF_Str, 255, ((uint8_t*)&speed2) + 2);
    RemoBF_get(&RemoBF_Str, 255, ((uint8_t*)&speed2) + 3);

    printf("%d , %d ,%d\n", PID2[0], PID2[1], PID2[2]);
    printf("%ld\n", speed2);
}
