/**
 * @file frequency_set.c
 * @author smallplayplay
 * @date 2021.5.27
 * @brief 降頻處理器
 */

#include "c4mlib/ServiceProvi/interrupt/src/frequency_set.h"

#include "c4mlib/C4MBios/debug/src/debug.h"
#include "c4mlib/C4MBios/macro/src/bits_op.h"
#include "c4mlib/C4MBios/macro/src/std_type.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdint.h>
#include <stdio.h>

#define FREQREDU_SET_OK                 0
#define FREQREDU_SET_Error              255
#define FREQREDU_SET_NULL_POINTER_ERROR 1
#define FREQREDU_SET_OVER_TASKID_ERROR  2

uint8_t FreqRedu_reg(FreqReduStr_t* FRSTR_ssp, TaskFunc_t FbFunc_p, void* FbPara_p,
                     uint8_t divi, uint8_t phase) {
    uint8_t TaskId     = FRSTR_ssp->Total;
    FRSTR_ssp->Counter = 0;
    FRSTR_ssp->Total++;
    FRSTR_ssp->Divi_p[TaskId].Divi      = divi;
    FRSTR_ssp->Divi_p[TaskId].DiviCount = 0;
    FRSTR_ssp->Divi_p[TaskId].phase     = phase;
    FRSTR_ssp->Task_p[TaskId].state     = 0;
    FRSTR_ssp->Task_p[TaskId].FBFunc_p  = FbFunc_p;
    FRSTR_ssp->Task_p[TaskId].FBPara_p  = FbPara_p;
    return TaskId;
}

uint8_t FreqRedu_en(FreqReduStr_t* FRSTR_ssp, uint8_t Task_Id, uint8_t Enable) {
    if (FRSTR_ssp == NULL) {
        return (FREQREDU_SET_NULL_POINTER_ERROR);
    }
    if (FRSTR_ssp->Total <= Task_Id) {
        return (FREQREDU_SET_OVER_TASKID_ERROR);
    }
    FRSTR_ssp->Task_p[Task_Id].state = Enable;
    return FREQREDU_SET_OK;
}

uint8_t FreqRedu_step(void* void_p) {
    FreqReduStr_t* FRSTR_ssp = (FreqReduStr_t*)void_p;
    cli();
    uint8_t count_flag = 1;
    if (FRSTR_ssp->HWRegByte == 1) {
        REGPUT(FRSTR_ssp->HWReg_p, FRSTR_ssp->HWRegByte,
               ((uint8_t*)(FRSTR_ssp->Period_p) + FRSTR_ssp->Counter));
    }
    else if (FRSTR_ssp->HWRegByte == 2) {
        REGPUT(FRSTR_ssp->HWReg_p, FRSTR_ssp->HWRegByte,
               ((uint16_t*)(FRSTR_ssp->Period_p) + FRSTR_ssp->Counter));
    }
    for (int i = 0; i < FRSTR_ssp->Total; i++) {
        if (FRSTR_ssp->Task_p[i].state == 1) {
            if ((FRSTR_ssp->Counter == FRSTR_ssp->Divi_p[i].phase)) {
                if (FRSTR_ssp->Divi_p[i].DiviCount == 0) {
                    FRSTR_ssp->Task_p[i].FBFunc_p(
                        FRSTR_ssp->Task_p[i].FBPara_p);
                }
            }
            if (FRSTR_ssp->Counter == (FRSTR_ssp->Cycle - 1)) {
                if (i == (FRSTR_ssp->Total) - 1) {
                    FRSTR_ssp->Counter = 0;
                    count_flag         = 0;
                }
                FRSTR_ssp->Divi_p[i].DiviCount++;
                if (FRSTR_ssp->Divi_p[i].DiviCount ==
                    FRSTR_ssp->Divi_p[i].Divi) {
                    FRSTR_ssp->Divi_p[i].DiviCount = 0;
                }
            }
        }
    }
    if (count_flag) {
        FRSTR_ssp->Counter++;
    }
    sei();
    return FREQREDU_SET_OK;
}
