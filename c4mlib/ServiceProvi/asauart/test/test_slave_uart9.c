/**
 * @file test_slave_uart9.c
 * @author Wu Cheng Han
 * @date 2019.10.07
 * @brief 測試 ASA單板電腦 作為Slave，建立遠端Register，當Master端
 * 透過Uart Mode9 跟遠端Register 通訊，進行封包傳送接收。
 *
 * 先低到高傳送資料會先低到高放入資料矩陣裡面，
 * 反之先高到低傳送資料會先高到低放入資料矩陣裡面。
 *
 * 需與 test_uart9 一起做測試
 * 原來:
 * reg_1[1] = {0};
 * reg_2[3] = {0, 0, 0};
 * reg_3[1] = {1};
 * reg_4[5] = {1, 2, 3, 4, 5};
 *
 * 執行結果:
 * reg_1[1] = {5};
 * reg_2[3] = {5, 6, 7};
 * reg_3[1] = {1};
 * reg_4[5] = {1, 2, 3, 4, 5};
 *
 * >>  reg_1[0] = 5
 * >>  reg_2[0] = 5  reg_2[1] = 6  reg_2[2] = 7
 * >>  reg_3[0] = 1
 * >>  reg_4[0] = 1  reg_4[1] = 2  reg_4[2] = 3  reg_4[3] = 4  reg_4[4] = 5
 */

#include "c4mlib/C4MBios/asabus/src/remo_reg.h"
#include "c4mlib/ServiceProvi/asauart/src/asauart_slave.h"
#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/C4MBios/hardware/src/hal_uart.h"
#include "c4mlib/C4MBios/hardware/src/isr.h"
#include "c4mlib/ServiceProvi/time/src/hal_time.h"
#include "c4mlib/ServiceProvi/time/src/timeout.h"

#include "c4mlib/config/remo_reg.cfg"

ISR(TIMER1_COMPA_vect) {
    Timeout_tick();
    HAL_tick();
}

ISR(USART1_RX_vect) {
    ASA_UARTS9_rx();
}
ISR(USART1_TX_vect) {
    ASA_UARTS9_tx_step();
}

// Initialize the TIME1 with CTC mode, interrupt at 1000 Hz
void init_timer();

int main() {
    // The configure program is same.
    C4M_DEVICE_set();
    init_timer();
    HAL_time_init();

    sei();

    SerialIsr_t UARTIsrStr = SERIAL_ISR_STR_UART_INI;
    // FIXEME: 使用新的初始化方式
    SerialIsr_net(&UARTIsrStr, 0);

    uint8_t reg_1[1] = {0};
    uint8_t reg_2[3] = {0, 0, 0};
    uint8_t reg_3[1] = {1};
    uint8_t reg_4[5] = {1, 2, 3, 4, 5};

    uint8_t reg_1_Id = RemoRW_reg(&UARTIsrStr, reg_1, 1);
    printf("Create RemoRWreg [%u] with %u bytes \n", reg_1_Id,
           UARTIsrStr.remo_reg[reg_1_Id].sz_reg);
    uint8_t reg_2_Id = RemoRW_reg(&UARTIsrStr, reg_2, 3);
    printf("Create RemoRWreg [%u] with %u bytes \n", reg_2_Id,
           UARTIsrStr.remo_reg[reg_2_Id].sz_reg);
    uint8_t reg_3_Id = RemoRW_reg(&UARTIsrStr, reg_3, 1);
    printf("Create RemoRWreg [%u] with %u bytes \n", reg_3_Id,
           UARTIsrStr.remo_reg[reg_3_Id].sz_reg);
    uint8_t reg_4_Id = RemoRW_reg(&UARTIsrStr, reg_4, 5);
    printf("Create RemoRWreg [%u] with %u bytes \n", reg_4_Id,
           UARTIsrStr.remo_reg[reg_4_Id].sz_reg);

    while (1) {
        for (int i = 0; i < 1; i++) {
            printf("reg_1[%d] = %3u  ", i, reg_1[i]);
        }
        printf("\n");

        for (int i = 0; i < 3; i++) {
            printf("reg_2[%d] = %3u  ", i, reg_2[i]);
        }
        printf("\n");

        for (int i = 0; i < 1; i++) {
            printf("reg_3[%d] = %3u  ", i, reg_3[i]);
        }
        printf("\n");

        for (int i = 0; i < 5; i++) {
            printf("reg_4[%d] = %3u  ", i, reg_4[i]);
        }
        printf("\n");
    }
}
void init_timer() {
    // Pre-scale 1
    // COMA COMB non-inverting mode
    // 1    0
    // CTC with TOP ICRn
    // WGM3 WGM2 WGM1 WGM0
    // 1    1    0    0
    // TCCR1A  [COM1A1 COM1A0 COM1B1 COM1B0 COM1C1 COM1C0 WGM11 WGM10]
    // TCCR1B  [ICNC1 ICES1 -- WGM13 WGM12 CS12 CS11 CS10]

    ICR1 = 11058;
    TCCR1A = 0b00000000;
    TCCR1B = 0b00011001;
    TCNT1 = 0x00;
    TIMSK |= 1 << OCIE1A;
}
