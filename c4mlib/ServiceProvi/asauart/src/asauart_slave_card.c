/**
 * @file asauart_slave_card.c
 * @author s915888
 * @date 2021.05.25
 * @brief 實現 ASA UART Slave 0 通訊函式
 */
#include "c4mlib/ServiceProvi/asauart/src/asauart_slave_card.h"

#include "c4mlib/C4MBios/asabus/src/asabus.h"
#include "c4mlib/C4MBios/asabus/src/remo_reg.h"
#include "c4mlib/C4MBios/debug/src/debug.h"
#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/C4MBios/macro/src/std_res.h"
#include "c4mlib/ServiceProvi/pipeline/src/pipeline_executor.h"
#include "c4mlib/ServiceProvi/pipeline/src/pipeline_imp.h"
#include "c4mlib/ServiceProvi/time/src/timeout.h"

#define UART_RXHEADER         0
#define UART_UID              1
#define UART_ADDR             2
#define UART_BYTES            3
#define UART_DATA             4
#define UART_RXCHKSUM         5
#define UART_RESP_OK          0
#define UART_RESP_TIMEOUT     199
#define UART_RESP_ERR         105
#define UART_WRITE_ADDR       0x80
#define UART_TRM              1
#define UART_REC              0
#define UART_DATA_COMPLETE    0
#define UART_TIMEOUT_START    0
#define UART_TIMEOUT_COUNTING 1
#define UART_TIMEOUT          2
#define UART_TIMECOUNT_ZERO   0
#define ENABLE                1
#define DISABLE               0
#define UART_TXHEADER         0
#define UART_RESP             1
#define UART_TXDATA           2
#define UART_TXCHKSUM         3
#define UART_TX_COMPLETE      4
#define COMPLETE              0
uint8_t UARTSRemoDD_step(void* void_p) {
    UARTSRemoStr_t* Str_p = (UARTSRemoStr_t*)void_p;
    RemoBF_put(Str_p->RemoBF_p, Str_p->RegAdd);
    return COMPLETE;
}

uint8_t UARTSRemoTO_step(void* void_p) {
    UARTSRemoStr_t* Str_p = (UARTSRemoStr_t*)void_p;
    switch (Str_p->TOState) {
        case UART_TIMEOUT_START:
            if (Str_p->RXState != UART_RXHEADER) {
                Str_p->TOState  = UART_TIMEOUT_COUNTING;
                Str_p->TimCount = Str_p->TimMax;
                HWInt_en(Str_p->TOIntStr_p, Str_p->TOTaskID, ENABLE);
            }
            break;
        case UART_TIMEOUT_COUNTING:
            Str_p->TimCount = Str_p->TimCount - 1;
            if (Str_p->TimCount == UART_TIMECOUNT_ZERO) {
                Str_p->TOState = UART_TIMEOUT;
                UARTSRemoRX_step(void_p);
            }
            break;
        case UART_TIMEOUT:
            if (Str_p->RXState == UART_RXHEADER) {
                Str_p->TOState = UART_TIMEOUT_START;
                HWInt_en(Str_p->TOIntStr_p, Str_p->TOTaskID, DISABLE);
            }
            break;
    }
    return COMPLETE;
}

uint8_t UARTSRemoTX_step(void* void_p) {
    uint8_t TXData = 0, Resid = 0;
    UARTSRemoStr_t* Str_p = (UARTSRemoStr_t*)void_p;
    switch (Str_p->TXState) {
        case UART_TXHEADER:
            TXData = Str_p->Header;
            REGPUT(Str_p->DataReg_p, 1, &TXData);
            Str_p->TXState = UART_RESP;
            break;
        case UART_RESP:
            TXData = Str_p->Resp;
            REGPUT(Str_p->DataReg_p, 1, &TXData);
            if (Str_p->TrmORec == UART_TRM) {
                Str_p->TXState = UART_TXCHKSUM;
            }
            else {
                Str_p->TXState = UART_TXDATA;
            }
            break;
        case UART_TXDATA:
            Resid         = RemoBF_get(Str_p->RemoBF_p, Str_p->RegAdd, &TXData);
            Str_p->ChkSum = Str_p->ChkSum + TXData;
            REGPUT(Str_p->DataReg_p, 1, &TXData);
            if (Resid == UART_DATA_COMPLETE) {
                Str_p->TXState = UART_TXCHKSUM;
            }
            break;
        case UART_TXCHKSUM:

            TXData = Str_p->ChkSum;
            REGPUT(Str_p->DataReg_p, 1, &TXData);
            Str_p->TXState = UART_TX_COMPLETE;
            break;
        case UART_TX_COMPLETE:
            Str_p->TXState = UART_TXHEADER;
            break;
    }
    return COMPLETE;
}

uint8_t UARTSRemoRX_step(void* void_p) {
    uint8_t RXData = 0, Resid = 0;
    UARTSRemoStr_t* Str_p = (UARTSRemoStr_t*)void_p;
    if (Str_p->TOState != UART_TIMEOUT) {
        REGGET(Str_p->DataReg_p, 1, &RXData);
        Str_p->TimCount = Str_p->TimMax;
    }
    if (Str_p->TOState == UART_TIMEOUT) {
        Str_p->Resp = UART_RESP_TIMEOUT;
        UARTSRemoTX_step(void_p);
        Str_p->RXState = UART_RXHEADER;
        UARTSRemoTO_step(void_p);
        return UART_TIMEOUT;
    }
    switch (Str_p->RXState) {
        case UART_RXHEADER:
            if (RXData == Str_p->Header) {
                Str_p->ChkSum  = RXData;
                Str_p->RXState = UART_UID;
                UARTSRemoTO_step(void_p);
            }
            break;
        case UART_UID:

            if (Str_p->UID == RXData) {
                Str_p->ChkSum  = Str_p->ChkSum + RXData;
                Str_p->RXState = UART_ADDR;
            }
            else {
                Str_p->RXState = UART_RXHEADER;
            }
            break;
        case UART_ADDR:

            if (RXData >= UART_WRITE_ADDR) {
                Str_p->TrmORec = UART_TRM;
                Str_p->RegAdd  = RXData - UART_WRITE_ADDR;
            }
            else {
                Str_p->TrmORec = UART_REC;
                Str_p->RegAdd  = RXData;
            }
            Str_p->ChkSum  = Str_p->ChkSum + RXData;
            Str_p->RXState = UART_BYTES;
            break;
        case UART_BYTES:
            Str_p->Byte   = RXData;
            Str_p->ChkSum = Str_p->ChkSum + RXData;
            if (Str_p->TrmORec == UART_TRM) {
                Str_p->RXState = UART_DATA;
            }
            else {
                Str_p->RXState = UART_RXCHKSUM;
            }
            break;
        case UART_DATA:
            Str_p->ChkSum = Str_p->ChkSum + RXData;
            Resid         = RemoBF_temp(Str_p->RemoBF_p, Str_p->RegAdd, RXData);
            if (Resid == UART_DATA_COMPLETE) {
                Str_p->RXState = UART_RXCHKSUM;
            }
            break;
        case UART_RXCHKSUM:
            if (RXData == Str_p->ChkSum) {
                Str_p->Resp = UART_RESP_OK;
                if (Str_p->TrmORec == UART_TRM) {
                    TRIG_NEXT_TASK(Str_p->DDTaskID);
                }
            }
            else {
                Str_p->Resp = UART_RESP_ERR;
                if (Str_p->TrmORec == UART_TRM) {
                    RemoBF_clr(Str_p->RemoBF_p);
                }
            }
            Str_p->RXState = UART_RXHEADER;
            UARTSRemoTX_step(void_p);
            break;
    }
    return COMPLETE;
}
