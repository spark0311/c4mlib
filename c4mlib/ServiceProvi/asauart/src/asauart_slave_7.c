/**
 * @file asauart_slave_7.c
 * @author Ye cheng-Wei
 * @author Wu Cheng Han
 * @date 2019.10.07
 * @brief 實現 ASA Slave UART Mode7 通運封包函式
 *
 * 提供 UART Slave端 Mode7 通訊封包函式 ASA_UARTS7_rx_step、ASA_UARTS7_tx_step，需先將ASA_UARTS7_rx_step
 * 註冊進Uart Rx中斷函式、ASA_UARTS7_tx_step 註冊進Uart Tx中斷函式。
 */

#include "c4mlib/C4MBios/asabus/src/asabus.h"
#include "c4mlib/C4MBios/asabus/src/remo_reg.h"
#include "c4mlib/C4MBios/debug/src/debug.h"
#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/C4MBios/macro/src/std_res.h"
#include "c4mlib/ServiceProvi/time/src/timeout.h"

#include "c4mlib/ServiceProvi/asauart/src/asauart_slave.h"

static uint8_t timeout_IsrID;
static uint8_t timeout_flag = 0;

void ASA_UARTS7_rx_step() {
    // If SerialIsr_init is not call first, There will return
    if (ASAUARTSerialIsrStr == NULL)
        return;

    DEBUG_INFO("ASA_UARTS7_rx_step call [Timeout:%u]\n", timeout_flag);
    uint8_t data_In;

    if (ASAUARTSerialIsrStr->sm_status == UARTS_SM_HEADER) {
        ASAUARTSerialIsrStr->sm_status =
            UARTS_SM_ADDR;  //狀態機沒有 header 狀態，直接跳至ADDR狀態
    }

    switch (ASAUARTSerialIsrStr->sm_status) {
        case UARTS_SM_ADDR:
            /*
                • TOCount=TOut;
                • 讀取UART收值，分解取bit7為RW，bit6:0為暫存器編號。
                • 檢查RW值決定切換狀態。(參考編號3 或4)
            */

            // 開啟逾時中斷
            Timeout_ctl(timeout_IsrID, 1);

            if (timeout_flag) {  // Timeout ISR，回ADDR
                ASAUARTSerialIsrStr->sm_status = UARTS_SM_ADDR;
                ASAUARTSerialIsrStr->result_message = HAL_ERROR_TIMEOUT;

                // 關閉逾時中斷
                Timeout_ctl(timeout_IsrID, 0);
                break;
            }

            UARTS_Inst.read_byte(&data_In,0);  // 讀取1byte資料。資料為WR+ADD
            DEBUG_INFO("[UARTS_SM_ADDR]\tread <%02X>\n", data_In);

            ASAUARTSerialIsrStr->reg_address =
                data_In & 0x7f;  // 取出資料內 ADD
            ASAUARTSerialIsrStr->rw_mode =
                (data_In & 0x80) >> 7;  // 取出資料內的 W/R

            if (ASAUARTSerialIsrStr->rw_mode == 0) {  // UART MODE5寫入模式
                ASAUARTSerialIsrStr->sm_status = UARTS_SM_DATA;
            }
            // 檢查RW值決定切換狀態 (參考編號3 或4)
            else if (ASAUARTSerialIsrStr->rw_mode == 1) {
                // UART MODE5 讀取模式
                DEBUG_INFO("Process read mode, ASA_UARTS7_tx_step ccall \n");
                ASAUARTSerialIsrStr->sm_status = UARTS_SM_BYTES;
                ASA_UARTS7_tx_step();
            }

            break;

        case UARTS_SM_DATA:
            /*
                • TOCount=TOut;
                • 讀取UART收值，轉存入BUFF(ByteCount  )中。
                • ByteCount =ByteCount +1
                • TotalBytes= RemoRW_reg表第暫存器編號個暫存器Byte數
                • If(ByteCount  ==TotalBytes) ByteCount =0
                • 檢查ByteCount 值決定切換狀態。
            */
            if (timeout_flag) {  // Timeout ISR，回ADDR
                ASAUARTSerialIsrStr->sm_status = UARTS_SM_ADDR;
                ASAUARTSerialIsrStr->result_message = HAL_ERROR_TIMEOUT;

                // 關閉逾時中斷
                Timeout_ctl(timeout_IsrID, 0);
                break;
            }

            UARTS_Inst.read_byte(&data_In,0);
            DEBUG_INFO("[UARTS_SM_DATA]\tread <%02X>\n", data_In);
            DEBUG_INFO(
                "byte_counter: <%u>, sz_reg: <%u>\n",
                ASAUARTSerialIsrStr->byte_counter,
                ASAUARTSerialIsrStr->remo_reg[ASAUARTSerialIsrStr->reg_address]
                    .sz_reg);
            ASAUARTSerialIsrStr->temp[ASAUARTSerialIsrStr->byte_counter] =
                data_In;
            ASAUARTSerialIsrStr->byte_counter++;

            if (ASAUARTSerialIsrStr->byte_counter ==
                ASAUARTSerialIsrStr->remo_reg[ASAUARTSerialIsrStr->reg_address]
                    .sz_reg) {
                ASAUARTSerialIsrStr->byte_counter = 0;

                DEBUG_INFO("Process write mode\n");
                DEBUG_INFO("ASAUARTSerialIsrStr addrewss:%x\n",
                           ASAUARTSerialIsrStr);
                // Move temporary data in buffer to target register memory
                for (int i = 0;
                     i < ASAUARTSerialIsrStr
                             ->remo_reg[ASAUARTSerialIsrStr->reg_address]
                             .sz_reg;
                     i++) {
                    DEBUG_INFO("remo_reg[%d]=%2u ; temp[%d]=%2u\n", i,
                               ASAUARTSerialIsrStr
                                   ->remo_reg[ASAUARTSerialIsrStr->reg_address]
                                   .data_p[i],
                               i, ASAUARTSerialIsrStr->temp[i]);
                    ASAUARTSerialIsrStr
                        ->remo_reg[ASAUARTSerialIsrStr->reg_address]
                        .data_p[i] = ASAUARTSerialIsrStr->temp[i];
                }
                // Process Modify event Callback
                if (ASAUARTSerialIsrStr
                        ->remo_reg[ASAUARTSerialIsrStr->reg_address]
                        .func_p != NULL)
                    ASAUARTSerialIsrStr
                        ->remo_reg[ASAUARTSerialIsrStr->reg_address]
                        .func_p(ASAUARTSerialIsrStr
                                    ->remo_reg[ASAUARTSerialIsrStr->reg_address]
                                    .funcPara_p);

                DEBUG_INFO("Process write register Done\n");

                ASAUARTSerialIsrStr->sm_status = UARTS_SM_ADDR;
                ASAUARTSerialIsrStr->byte_counter = 0;
                // 關閉逾時中斷
                Timeout_ctl(timeout_IsrID, 0);
            }

            break;
    }

    // Reset the timeout ISR
    TimerCntStr_inst.timeoutISR_inst[timeout_IsrID].counter =
        TimerCntStr_inst.timeoutISR_inst[timeout_IsrID].time_limit;
    timeout_flag = 0;
}

void ASA_UARTS7_tx_step() {
    DEBUG_INFO("ASA_UARTS7_tx_step call\n");
    switch (ASAUARTSerialIsrStr->sm_status) {
        case UARTS_SM_BYTES:

            /**** Write the Data to Master ****/
            UARTS_Inst.write_byte(
                ASAUARTSerialIsrStr->remo_reg[ASAUARTSerialIsrStr->reg_address]
                    .data_p[ASAUARTSerialIsrStr->byte_counter],0);
            ASAUARTSerialIsrStr->byte_counter++;
            // 當 byte_counter == 資料大小
            if (ASAUARTSerialIsrStr->byte_counter ==
                ASAUARTSerialIsrStr->remo_reg[ASAUARTSerialIsrStr->reg_address]
                    .sz_reg) {
                ASAUARTSerialIsrStr->sm_status =
                    UARTS_SM_ADDR;                      // 狀態機切回 ADDR
                ASAUARTSerialIsrStr->byte_counter = 0;  // byte_counter 重製歸0
            }
            // 關閉逾時中斷
            Timeout_ctl(timeout_IsrID, 0);
            break;
    }
}
