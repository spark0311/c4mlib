/**
 * @file test_HWInt.c
 * @author ya058764,smallplayplay
 * @date 2020.05.13,2021.05.21
 * @brief 以外部中斷代為測試，先檢查連結無誤後再用外部中斷觸發兩個userfunction
 * 
 **/

#include "c4mlib/ServiceProvi/hwimp/src/ext_imp.h"
#include "c4mlib/ServiceProvi/hwimp/src/hardware_interrupt.h"
#include "c4mlib/ServiceProvi/hwimp/src/layout_macro.h"
#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/C4MBios/hardwareset/src/ext_set.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdio.h>
#define PERIPHERAL_INTERRUPT_ENABLE 255

uint8_t isr1(void *);
uint8_t isr2(void *);
struct para {
    int A;
    int B;
} ISRpara;

int main() {
    C4M_DEVICE_set();
    DDRE = 0;
    EIMSK = 0x01 << 4;
    EICRB = 0x03;
    ISRpara.A = ISRpara.B = 0;
    printf("===setting===\n");
    /*TaskBlock_t Ext4Report[2];
    HWIntStr_t Ext4ISR = {.MaxTask = 2, .Total = 0, .Task_p = Ext4Report};
    ExtIntStrList_p[4] = &Ext4ISR;*/
    EXTOpStr_t HWOPSTR = EXT4OPSTRINI;
    EXTHWINT_LAY(Ext4ISR,4,2,HWOPSTR);
    uint8_t TaskID = HWInt_reg(&Ext4ISR, &isr1, &ISRpara);
    HWInt_en(&Ext4ISR, PERIPHERAL_INTERRUPT_ENABLE, ENABLE);
    HWInt_en(&Ext4ISR, TaskID, 1);
    //EXTHWINT_REG(Ext4ISR,&isr1,ISRpara,1);
    uint8_t TaskID2 = HWInt_reg(&Ext4ISR, &isr2, &ISRpara);
    HWInt_en(&Ext4ISR, TaskID2, 1);
    //EXTHWINT_REG(Ext4ISR,&isr2,ISRpara,1);
    printf("HWIntStr : .Total = %d .Task_p = %p\n", Ext4ISR.Total,
           Ext4ISR.Task_p);
    printf("TaskBlock : 1.state = %d 2.state = %d \n", Ext4ISR.Task_p[0].state,
           Ext4ISR.Task_p[1].state);
    printf("TaskBlock : 1.userfunction = %p 2.userfunction = %p \n",
           Ext4ISR.Task_p[0].FBFunc_p, Ext4ISR.Task_p[1].FBFunc_p);
    printf("1.userfunction: %p 2.userfunction: %p\n",&isr1,&isr2);
    sei();
    printf("===start===\n");
    while(1){

    }
}

uint8_t isr1(void *para) {
    struct para *A = (struct para *)para;
    printf("A = %d\n", A->A++);
    return 0;
}

uint8_t isr2(void *para) {
    struct para *B = (struct para *)para;
    printf("B = %d\n", B->B++);
    return 0;
}
