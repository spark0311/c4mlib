/**
 * @file ext_imp.h
 * @author LiYu87
 * @date 2019.09.04
 * @brief
 *
 */

#ifndef C4MLIB_HARDWARE2_EXT_IMP_H
#define C4MLIB_HARDWARE2_EXT_IMP_H

#if defined(__AVR_ATmega128__)
#    include "m128/ext_imp.h"
#elif defined(__AVR_ATmega88__) || defined(__AVR_ATmega48__) || \
    defined(__AVR_ATmega168__)
#    include "m88/ext_imp.h"
#elif defined(__AVR_ATtiny2313__)
#    include "tiny2313/ext_imp.h"
#else
#    if !defined(__COMPILING_AVR_LIBC__)
#        warning "device type not defined"
#    endif
#endif

#endif  // C4MLIB_HARDWARE2_EXT_IMP_H
