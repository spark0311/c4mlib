/**
 * @file test_pipline.c
 * @author cy023, ya058764
 * @date 2021.5.30
 * @brief
 *
 */
#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/C4MBios/hardwareset/src/tim_set.h"
#include "c4mlib/ServiceProvi/hwimp/src/layout_macro.h"
#include "c4mlib/ServiceProvi/hwimp/src/tim_imp.h"
#include "c4mlib/ServiceProvi/pipeline/src/pipeline_executor.h"
#include "c4mlib/ServiceProvi/pipeline/src/pipeline_imp.h"

#include <stdio.h>
#include <string.h>

#include "c4mlib/ServiceProvi/pipeline/test/test_pipeline_tim.cfg"

#define TASK1                       1
#define TASK2                       2
#define TASK3                       3
#define PERIPHERAL_INTERRUPT_ENABLE 255

typedef struct {
    int p;
} FuncParaStr_t;

uint8_t Task1(void *void_p);
uint8_t Task2(void *void_p);
uint8_t Task3(void *void_p);

uint8_t TIM2_init(void) {
    HardWareSet_t TIM2HWSet_str      = TIM2HWSETSTRINI;
    HWFlagPara_t TIM2FgGpData_str[5] = TIM2FLAGPARALISTINI;
    HWRegPara_t TIM2RegData_str      = TIM2REGPARALISTINI;
    TIM2HWSetDataStr_t TIM2HWSetData = TIM2SETDATALISTINI;
    HARDWARESET_LAY(TIM2HWSet_str, TIM2FgGpData_str[0], TIM2RegData_str,
                    TIM2HWSetData)
    return HardwareSet_step(&TIM2HWSet_str);
}

int main() {
    C4M_DEVICE_set();
    printf("========== start ==========\n");
    // hardware initial section start
    TIM2_init();
    TIMOpStr_t HWOPSTR = TIM2OPSTRINI;
    TIMHWINT_LAY(timer2_str, 2, 1, HWOPSTR);
    PIPELINE_LAY(3, 3, 10);
    // hardware initial section end

    // hardware interrupt regist section start
    uint8_t taskID = HWInt_reg(&timer2_str, &Pipeline_step, &SysPipeline_str);
    HWInt_en(&timer2_str, PERIPHERAL_INTERRUPT_ENABLE, ENABLE);
    HWInt_en(&timer2_str, taskID, ENABLE);
    // hardware interrupt regist section end

    // pipeline regist section start
    FuncParaStr_t Task1_para = {.p = 0};
    FuncParaStr_t Task2_para = {.p = 0};
    FuncParaStr_t Task3_para = {.p = 0};

    uint8_t task1_name[] = "Task1\0";
    uint8_t task2_name[] = "Task2\0";
    uint8_t task3_name[] = "Task3\0";

    Pipeline_reg(&SysPipeline_str, &Task1, &Task1_para, task1_name);
    Pipeline_reg(&SysPipeline_str, &Task2, &Task2_para, task2_name);
    Pipeline_reg(&SysPipeline_str, &Task3, &Task3_para, task3_name);
    // pipeline regist section end

    TRIG_NEXT_TASK(TASK1);
    sei();
    while (1)
        ;

    return 0;
}

uint8_t Task1(void *void_p) {
    FuncParaStr_t *Para_p = (FuncParaStr_t *)void_p;
    Para_p->p             = Para_p->p + 1;
    printf("\nTask1 Para_p->p = %d\n", Para_p->p);
    TRIG_NEXT_TASK(TASK2);
    return 0;
}

uint8_t Task2(void *void_p) {
    FuncParaStr_t *Para_p = (FuncParaStr_t *)void_p;
    Para_p->p             = Para_p->p + 2;
    printf("\nTask2 Para_p->p = %d\n", Para_p->p);
    TRIG_NEXT_TASK(TASK3);
    return 0;
}

uint8_t Task3(void *void_p) {
    FuncParaStr_t *Para_p = (FuncParaStr_t *)void_p;
    Para_p->p             = Para_p->p + 3;
    printf("\nTask3 Para_p->p = %d\n", Para_p->p);
    TRIG_NEXT_TASK(TASK1);
    return 0;
}
