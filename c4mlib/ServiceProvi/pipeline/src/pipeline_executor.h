/**
 * @file pipeline_executor.h
 * @author cy023, ya058764
 * @date 2021.5.30
 * @priority 1
 * @brief
 */
#ifndef PIPELINE_SET_H
#define PIPELINE_SET_H

#include "c4mlib/C4MBios/macro/src/std_type.h"
#include "c4mlib/ServiceProvi/databuffer/src/fifobf.h"
#include "c4mlib/ServiceProvi/databuffer/src/scope.h"
#include "c4mlib/ServiceProvi/hwimp/src/hardware_interrupt.h"

#include <stdint.h>
#include <string.h>

/* Public Section Start */

/**
 * @brief   Pipeline 結構體
 * @ingroup SysPipeline_struct
 *
 * @param MaxTask Maximum capacity for tasks to be Registed
 * @param Total Total number of Registed Tasks
 * @param TaskId Task Identification gotten
 * @param HWIStr_ssp pointer of HW interrupt structure
 * @param Task_p pointer of Task List
 * @param FifoBF_p pointer of FIFO buffer structure
 * @param ScopeBF_p pointer of scope buffer structure
 */
typedef struct {
    uint8_t MaxTask;
    uint8_t Total;
    volatile uint8_t TaskId;
    HWIntStr_t* HWIStr_ssp;
    TaskBlock_t* Task_p;
    FifoBFStr_t* FifoBF_p;
    ScopeBFStr_t* ScopeBF_p;
} PipelineStr_t;

/**
 * @brief
 * 供使用者呼叫將功能方塊登錄成可由管道工作執行器代執行的工作方塊。執行工作包括:
 *          1. 取得新工作編號。
 *          2. 將功能方塊結構體指標，步級函式，存進管道工作結構體相應編號位置。
 *          3. 將管道工作結構體，工作編號，存進功能方塊結構體。
 *          4. 試跑工作方塊，求取執行需時，送到人機顯示。
 *
 * @param PLStr_ssp     管道工作執行器管理用結構體的住址指標。
 * @param FbFunc_p      管道工作執行器執行之工作函式之住址指標。
 * @param FbPara_p      管道工作執行器執行之工作函式專用結構體之住址指標。
 * @param TaskName_p    pointer to Name of the task to be registered 工作名稱。
 * @return              工作識別碼。
 */
uint8_t Pipeline_reg(PipelineStr_t* PLStr_ssp, TaskFunc_t FbFunc_p, void* FbPara_p,
                     uint8_t* TaskName_p);

/**
 * @brief
 * 能夠由先進先出緩衝器取得工作識別碼，據以由工作方塊表中取得工作方塊並予以執行。
 *        本身亦與管道工作結構體組成工作方塊並登錄於計時中斷中執行。
 *        完成執行之工作識別碼會存入移動區段緩衝區做為記錄，若工作方塊回應有錯誤時，
 *        除了會將錯誤訊息工作名稱送往人機之外，也將先前執行的工作識別碼送往人機，以協助除錯。
 *
 * @param void_p 己被無形化之管道工作執行器管理用結構體。
 */
uint8_t Pipeline_step(void* void_p);

/**
 * @brief   提供管道工作執行器初始化所需之設定佈局
 * @ingroup pipeline_set_macro
 *
 * @param TASKNUM       Maximum Number of Tasks allow to be registed into
 * Pipeline executor.
 * @param FBDEPTH       FIFO Buffer element number.
 * @param SBDEPTH       Scope Buffer element number.
 */
#define PIPELINE_LAY(TASKNUM, FBDEPTH, SBDEPTH)                                \
    FIFOBUFF_LAY(PipelineFifo_str, FBDEPTH);                                   \
    SCOPEBUFF_LAY(PipelineScope_str, SBDEPTH, SBDEPTH);                        \
    {                                                                          \
        static TaskBlock_t Pipeline_TASKLIST[TASKNUM + 1];                     \
        SysPipeline_str.MaxTask   = TASKNUM;                                   \
        SysPipeline_str.Task_p    = Pipeline_TASKLIST;                         \
        SysPipeline_str.FifoBF_p  = &PipelineFifo_str;                         \
        SysPipeline_str.ScopeBF_p = &PipelineScope_str;                        \
    }

/**
 * @brief
 * 提供前級工作方塊，於完成本身工作之後，叫用以便將其後級的後續工作指標，放進先進出緩衝區等待管道工作執行器執行。
 * @ingroup trig_next_task_macro
 *
 * @param NEXTTASKINDEX Index of next Tasks.
 */
#define TRIG_NEXT_TASK(NEXTTASKINDEX)                                          \
    {                                                                          \
        SysPipeline_str.TaskId =                                               \
            NEXTTASKINDEX % (SysPipeline_str.MaxTask + 1);                     \
        SysPipeline_str.Task_p[SysPipeline_str.TaskId].state++;                \
        FifoBF_put((FifoBFStr_t*)(SysPipeline_str.FifoBF_p),                   \
                   (void*)&SysPipeline_str.TaskId, 1);                         \
    }

/* Public Section End */

#endif  // PIPELINE_SET_H
