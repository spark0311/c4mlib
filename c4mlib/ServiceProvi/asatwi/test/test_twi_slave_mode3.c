/**
 * @file test_twi_slave_mode3.c
 * @author YuChen
 * @brief TWI Slave mode3測試程式
 * @date 2019.10.04
 *
 * 測試方式：<br>
 *      軟體：本測試程式需搭配test_twi_master_mode3.c同步測試。<br>
 *      硬體：須將兩塊ASA_M128單板電腦的SCL與SDA串接。<br>
 * 測試時須先將已燒錄test_twi_slave_mode3.c的ASA_M128啟動，再將燒錄test_twi_master_mode3.c的ASA_M128啟動。<br>
 *
 * 測試程式執行動作：<br>
 * 接收Master板中的資料，並回傳。<br>
 * 傳輸資料：將接收的資料各加10。<br>
 * 接收資料：Master端的資料。
 *
 * 測試結果：<br>
 * MASTER傳輸前：<br>
 * >>  MODE3 TRAMSMIT TEST<br>
 * >>  Create RemoRWreg [2] with 10 bytes<br>
 * MASTER傳輸後：<br>
 * >>  [slave] Receive mode3 Reg_1[0]=10<br>
 * >>  [slave] Receive mode3 Reg_1[1]=11<br>
 * >>  [slave] Receive mode3 Reg_1[2]=12<br>
 * >>  [slave] Receive mode3 Reg_1[3]=13<br>
 * >>  [slave] Receive mode3 Reg_1[4]=14<br>
 * >>  [slave] Receive mode3 Reg_1[5]=15<br>
 * >>  [slave] Receive mode3 Reg_1[6]=16<br>
 * >>  [slave] Receive mode3 Reg_1[7]=17<br>
 * >>  [slave] Receive mode3 Reg_1[8]=18<br>
 * >>  [slave] Receive mode3 Reg_1[9]=19
 */

#include "c4mlib/C4MBios/asabus/src/remo_reg.h"
#include "c4mlib/ServiceProvi/asatwi/src/asa_twi.h"
#include "c4mlib/ServiceProvi/asatwi/src/twi.h"
#include "c4mlib/C4MBios/device/src/device.h"

#include <avr/interrupt.h>

#include "c4mlib/config/remo_reg.cfg"

void TWI_Slave_set();
#define Slave_ADDRESS 0x39
int main(void) {
    C4M_STDIO_init();
    C4M_DEVICE_set();
    TWI_Slave_set(); /*Slave Setting*/
    printf("MODE3 TRAMSMIT TEST\n");
    SerialIsr_t TWIIsrStr =
        SERIAL_ISR_STR_TWI_INI; /*初始化TWI Remo_reg結構資料*/
    // FIXME: 使用新版初始化
    SerialIsr_net(&TWIIsrStr, 0);
    uint8_t reg_1[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; /*建立Slave端暫存器*/
    uint8_t reg_1_ID =
        RemoRW_reg(&TWIIsrStr, reg_1,
                   10); /*註冊暫存器並取得暫存器ID(對應Master函式RegAdd傳參)*/
    printf("Create RemoRWreg [%u] with %u bytes\n", reg_1_ID,
           TWIIsrStr.remo_reg[reg_1_ID].sz_reg);
    sei(); /*Enable interrup*/
    while (1) {
        if (reg_1[9] == 19) {
            for (int i = 0; i < 10; i++) {
                printf("[slave] Receive mode3 Reg_1[%d]=%d\t\n", i, reg_1[i]);
                reg_1[i] += 10;
            }
        }
        _delay_ms(100);
    }
}
ISR(TWI_vect) {
    ASA_TWIS3_step();
}

void TWI_Slave_set() {
    TWAR = Slave_ADDRESS << 1;
    // Set TWI speed // _CPU Clock frequency_/16+2*(TWBR)*4^(prescaler bits) ;
    // prescaler bits = 1
    TWBR = 12;
    // enable TWI TWI_Interrupt and shack_hand for Master Start signal
    TWCR = (1 << TWEN) | (1 << TWIE) | (1 << TWEA);
}
