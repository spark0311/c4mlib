/**
 * @file test_master_mem_rec_mode5.c
 * @author Deng Xiang-Guan
 * @date 2019.10.03
 * @brief 測試SPIM_Mem_rec mode 5函式。
 * 
 * 需搭配test_slave_mem_rec_mode5.c，測試SPI Master(主)記憶接收資料，
 * 由低到高，接收來自Slave端的資料，測試方式如下條列表示：
 *  1. 使用SPIM_Mem_rec的函式，和Slave端搭配command、memory address，之後開始接收Slave端的資料。
 *  2. 檢查接收的資料和測試資料是否正確。
 *  3. 正確的話將成功計數器增加。
 *  4. 更新測試資料，回到流程1。
 */

#include "c4mlib/C4MBios/asabus/src/asabus.h"
#include "c4mlib/C4MBios/asabus/src/pin_def.h"
#include "c4mlib/ServiceProvi/asaspi/src/asaspi_master.h"
#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/C4MBios/hardware/src/hal_spi.h"

#define SPI_MODE 5
#define ASAID 4

#define test_command 0x02

uint32_t mem_addr = 12341487;
uint8_t test_data[5] = {0, 0, 0, 0, 0};
uint8_t ans_data[5] = {0, 1, 2, 3, 4};

int main() {
    // Setup
    uint16_t success_cnt = 0;
    C4M_STDIO_init();
    SPIM_Inst.init();
    printf("Start test SPIM_Mem_rec mode 5\n");
    while(true) {
        uint8_t cnt = 0;
        SPIM_Mem_rec(SPI_MODE, ASAID, test_command, 4, &mem_addr, sizeof(test_data), test_data);
        printf("data[0]:%d, data[1]:%d, data[2]:%d, data[3]:%d, data[4]:%d\n", test_data[0], test_data[1], test_data[2], test_data[3], test_data[4]);
        for(uint8_t i=0;i<4;i++) {
            if(test_data[i] == ans_data[i]) {
                cnt++;
                ans_data[i] += sizeof(ans_data);
            }
        }
        if(cnt == 4) {
            success_cnt++;
        }
        if(success_cnt != 0) {
            printf("Success !!! count:%d\n", success_cnt);
        }
        _delay_ms(3000);
    }

}
