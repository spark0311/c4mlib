/**
 * @file hal_twi.c
 * @author LiYu87
 * @date 2019.07.15
 * @brief twi 硬體抽象層 各硬體實作分割。
 */

#if defined(__AVR_ATmega128__)
#    include "m128/hal_twi.c"
#elif defined(__AVR_ATmega88__) || defined(__AVR_ATmega48__) || \
    defined(__AVR_ATmega168__)
#    include "m88/hal_twi.c"
#elif defined(__AVR_ATtiny2313__)
#    include "tiny2313/hal_twi.c"
#else
#    if !defined(__COMPILING_AVR_LIBC__)
#        warning "device type not defined"
#    endif
#endif
