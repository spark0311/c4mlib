/**
 * @file hal_twi.h
 * @author Deng Xiang-Guan
 * @date 2019.07.22
 * @brief Create a hardware abstraction layer to separate hardware dependent.
 */

#ifndef C4MLIB_HARDWARE_HAL_TWI_H
#define C4MLIB_HARDWARE_HAL_TWI_H

#include <stdint.h>

#endif /* C4MLIB_HARDWARE_HAL_TWI_H */
