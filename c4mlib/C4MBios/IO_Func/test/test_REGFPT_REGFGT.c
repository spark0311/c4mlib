/**
 * @file test_REGFPT_REGFGT.c
 * @author Wei (w8indow61231111@gmail.com)
 * @date 2020.07.28
 * @brief 測試 REGFPT, REGFGT 第一引數 變數 改 指標
 *
 */

#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/C4MBios/IO_Func/src/dio.h"

#include <avr/io.h>

int main() {
    C4M_DEVICE_set();
    printf("DDRA=DDRA,REGFGT(data_p) \n");
    uint8_t data_p = 0xff;
    REGFPT(&DDRA, 255, 0, 255);
    REGFGT(&DDRA,255,0,&data_p);
    printf("DDRA=%x,%x \n",DDRA,data_p);

    REGFPT(&PORTA, 255, 0, 255);
    REGFGT(&PORTA,255,0,&data_p);
    printf("PORTA=%x,%x \n",PORTA,data_p);

    REGFPT(&PINA, 255, 0, 255);
    REGFGT(&PINA,255,0,&data_p);
    printf("PINA=%x,%x \n",PINA,data_p);

    REGFPT(&ADCSRA, 255, 0, 255);
    REGFGT(&ADCSRA,255,0,&data_p);
    printf("ADCSRA=%x,%x \n",ADCSRA,data_p);   

    REGFPT(&TCCR2, 255, 0, 255);
    REGFGT(&TCCR2,255,0,&data_p);
    printf("TCCR2=%x,%x \n",TCCR2,data_p);      

}
