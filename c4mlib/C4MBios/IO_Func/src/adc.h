/**
 * @file adc.h
 * @author s915888
 * @date 2020.4.24
 * @author ya058764
 * @date 2021.06.05
 * @brief 宣告 adc 暫存器操作函式原型。
 */

#ifndef C4MLIB_IOFUNC_ADC_H
#define C4MLIB_IOFUNC_ADC_H
#include <stdint.h>
/**
 * @defgroup hw_adc_func hardware adc functions
 */

/* Public Section Start */
/**
 * @brief adc flag put 函式
 *
 * @ingroup hw_adc_func
 * @param REG_p 暫存器位址。
 * @param Mask   遮罩。
 * @param Shift  向左位移。
 * @param Data   寫入資料。
 * @return char 錯誤代碼：
 *   - 0：成功無誤。
 *   - 2：參數 *REG_p 錯誤。
 *   - 5：參數 Shift 超出範圍。
 */

uint8_t ADC_fpt(volatile unsigned char *REG_p, uint8_t Mask, uint8_t Shift, uint8_t Data);

/**
 * @brief adc flag get 函式
 *
 * @ingroup hw_adc_func
 * @param REG_p 暫存器位址。
 * @param Mask   遮罩。
 * @param Shift  向左位移。
 * @param Data   寫入資料。
 * @return char 錯誤代碼：
 *   - 0：成功無誤。
 *   - 2：參數 *REG_p 錯誤。
 *   - 5：參數 Shift 超出範圍。
 */
uint8_t ADC_fgt(volatile unsigned char *REG_p, uint8_t Mask, uint8_t Shift,
             void *Data_p);

/**
 * @brief adc put 函式
 *
 * @ingroup hw_adc_func
 * @param REG_p 暫存器位址。
 * @param Bytes  資料大小。
 * @param Data   寫入資料。
 * @return char 錯誤代碼：
 *   - 0：成功無誤。
 *   - 2：參數 *REG_p 錯誤。
 *   - 3：參數 Bytes 錯誤。
 */
uint8_t ADC_put(volatile unsigned char *REG_p, uint8_t Bytes, void *Data_p);

/**
 * @brief adc get 函式
 *
 * @ingroup hw_adc_func
 * @param REG_p 暫存器位址。
 * @param Bytes  資料大小。
 * @param Data   寫入資料。
 * @return char 錯誤代碼：
 *   - 0：成功無誤。
 *   - 2：參數 *REG_p 錯誤。
 *   - 3：參數 Bytes 錯誤。
 */
uint8_t ADC_get(volatile unsigned char *REG_p, uint8_t Bytes, void *Data_p);

/**
 * @brief adc set 函式
 *
 * @ingroup hw_adc_func
 * @param REG_p 暫存器位址。
 * @param Mask   遮罩。
 * @param Shift  向左位移。
 * @param Data   寫入資料。
 * @return char 錯誤代碼：
 *   - 0：成功無誤。
 *   - 2：參數 *REG_p 錯誤。
 *   - 5：參數 Shift 超出範圍。
 */
uint8_t ADC_set(volatile unsigned char *REG_p, uint8_t Mask, uint8_t Shift, uint8_t Data);

/* Public Section End */

#endif  // C4MLIB_IOFUNC_ADC_H
