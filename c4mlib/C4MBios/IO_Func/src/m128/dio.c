/**
 * @file dio.c
 * @author LiYu87
 * @author ya058764
 * @date 2019.07.29
 * @author ya058764
 * @date 2021.06.05
 * @brief dio 及 ext 暫存器操作相關函式實作。
 *
 * EXT類函式為DIO的重名，目的是為了與舊版本規格書相容。(2019.06.24)
 */

#include "c4mlib/C4MBios/IO_Func/src/dio.h"

#include "c4mlib/C4MBios/macro/src/bits_op.h"
#include "c4mlib/C4MBios/macro/src/std_res.h"

#include <avr/io.h>

#define RES_WARNING_IO_CONFLICT 6

uint8_t DIO_fpt(volatile unsigned char *REG_p, uint8_t Mask, uint8_t Shift, uint8_t Data_p);
uint8_t DIO_fgt(volatile unsigned char *REG_p, uint8_t Mask, uint8_t Shift,
                void *Data_p);
uint8_t DIO_put(volatile unsigned char *REG_p, uint8_t Bytes, void *Data_p);
uint8_t DIO_get(volatile unsigned char *REG_p, uint8_t Bytes, void *Data_p);

uint8_t EXT_fpt(volatile unsigned char *REG_p, uint8_t Mask, uint8_t Shift, uint8_t Data)
    __attribute__((alias("DIO_fpt")));

uint8_t EXT_fgt(volatile unsigned char *REG_p, uint8_t Mask, uint8_t Shift, void *Data_p)
    __attribute__((alias("DIO_fgt")));

uint8_t DIO_fpt(volatile unsigned char *REG_p, uint8_t Mask, uint8_t Shift,
             uint8_t Data_p) {
    if (REG_p == &PORTA || REG_p == &PORTB || REG_p == &PORTC ||
        REG_p == &PORTD || REG_p == &PORTE || REG_p == &PORTF ||
        REG_p == &PORTG || REG_p == &DDRA || REG_p == &DDRB || REG_p == &DDRC ||
        REG_p == &DDRD || REG_p == &DDRE || REG_p == &DDRF || REG_p == &DDRG ||
        REG_p == &EIMSK || REG_p == &EICRA || REG_p == &EICRB ||
        REG_p == &EIFR) {
        if (Shift <= 7) {
            REGFPT(REG_p, Mask, Shift, Data_p);
            if ((Mask & *REG_p) == Mask) {
                return RES_OK;
            }
            else {
                return RES_WARNING_IO_CONFLICT;
            }
        }
        else {
            return RES_ERROR_SHIFT;
        }
    }
    else {
        return RES_ERROR_LSBYTE;
    }
    return RES_OK;
}

uint8_t DIO_fgt(volatile unsigned char *REG_p, uint8_t Mask, uint8_t Shift,
                void *Data_p) {
    if (REG_p == &PINA || REG_p == &PINB || REG_p == &PINC || REG_p == &PIND ||
        REG_p == &PINE || REG_p == &PINF || REG_p == &PING || REG_p == &EIMSK ||
        REG_p == &EICRA || REG_p == &EICRB || REG_p == &EIFR) {
        if (Shift <= 7) {
            REGFGT(REG_p, Mask, Shift, Data_p);
            if (((~*REG_p) & Mask) == Mask) {
                return RES_OK;
            }
            else {
                return RES_WARNING_IO_CONFLICT;
            }
        }
        else {
            return RES_ERROR_SHIFT;
        }
    }
    else {
        return RES_ERROR_LSBYTE;
    }
    return RES_OK;
}

uint8_t DIO_put(volatile unsigned char *REG_p, uint8_t Bytes, void *Data_p) {
    if (REG_p == &PORTA || REG_p == &PORTB || REG_p == &PORTC ||
        REG_p == &PORTD || REG_p == &PORTE || REG_p == &PORTF ||
        REG_p == &PORTG || REG_p == &DDRA || REG_p == &DDRB || REG_p == &DDRC ||
        REG_p == &DDRD || REG_p == &DDRE || REG_p == &DDRF || REG_p == &DDRG) {
        if (Bytes == 1) {
            *REG_p = *((char *)Data_p);
            if (*REG_p == 0xff) {
                return RES_OK;
            }
            else {
                return RES_WARNING_IO_CONFLICT;
            }
        }
        else {
            return RES_ERROR_BYTES;
        }
    }
    else {
        return RES_ERROR_LSBYTE;
    }
    return RES_OK;
}

uint8_t DIO_get(volatile unsigned char *REG_p, uint8_t Bytes, void *Data_p) {
    if (REG_p == &PINA || REG_p == &PINB || REG_p == &PINC || REG_p == &PIND ||
        REG_p == &PINE || REG_p == &PINF || REG_p == &PING) {
        if (Bytes == 1) {
            *((char *)Data_p) = *REG_p;
            if (*REG_p == 0x00) {
                return RES_OK;
            }
            else {
                return RES_WARNING_IO_CONFLICT;
            }
        }
        else {
            return RES_ERROR_BYTES;
        }
    }
    else {
        return RES_ERROR_LSBYTE;
    }
    return RES_OK;
}
