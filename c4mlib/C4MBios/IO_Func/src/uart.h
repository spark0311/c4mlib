/**
 * @file uart.h
 * @author LiYu87
 * @date 2019.04.06
 * @author ya058764
 * @date 2021.06.05
 * @brief 宣告 uart 暫存器操作函式原型。
 */

#ifndef C4MLIB_IOFUNC_UART_H
#define C4MLIB_IOFUNC_UART_H
#include <stdint.h>
/**
 * @defgroup hw_uart_func hardware uart functions
 * @ingroup hw_uart
 */

/* Public Section Start */
/**
 * @brief UART flag put函式
 *
 * @ingroup hw_uart_func
 * @param REG_p 暫存器編號。
 * @param Mask   遮罩。
 * @param Shift  向左位移。
 * @param Data   寫入資料。
 * @return char 錯誤代碼：
 *   - 0：成功無誤。
 *   - 2：參數 REG_p 錯誤。
 *   - 4：參數 Mask 錯誤。
 *   - 5：參數 Shift 超出範圍。
 */
uint8_t UART_fpt(volatile unsigned char *REG_p, uint8_t Mask, uint8_t Shift,
                 uint8_t Data);

/**
 * @brief ASA UART flag get函式
 *
 * @ingroup hw_uart_func
 * @param REG_p 暫存器編號。
 * @param Mask   遮罩。
 * @param Shift  向右位移。
 * @param Data_p 資料指標。
 * @return char 錯誤代碼：
 *   - 0：成功無誤。
 *   - 2：參數 REG_p 錯誤。
 *   - 4：參數 Mask 錯誤。
 *   - 5：參數 Shift 超出範圍。
 */
uint8_t UART_fgt(volatile unsigned char *REG_p, uint8_t Mask, uint8_t Shift,
                 void *Data_p);

/**
 * @brief ASA UART put函式
 *
 * @ingroup hw_uart_func
 * @param REG_p 暫存器編號。
 * @param Bytes  資料大小。
 * @param Data_p 資料指標。
 * @return char  錯誤代碼：
 *   - 0：成功無誤。
 *   - 3：參數 Bytes 錯誤。
 */
uint8_t UART_put(volatile unsigned char *REG_p, uint8_t Bytes, void *Data_p);

/**
 * @brief ASA UART get函式
 *
 * @ingroup hw_uart_func
 * @param REG_p 暫存器編號。
 * @param Bytes  資料大小。
 * @param Data_p 資料指標。
 * @return char  錯誤代碼：
 *   - 0：成功無誤。
 *   - 3：參數 Bytes 錯誤。
 */
uint8_t UART_get(volatile unsigned char *REG_p, uint8_t Bytes, void *Data_p);
/* Public Section End */

#endif  // C4MLIB_IOFUNC_UART_H
