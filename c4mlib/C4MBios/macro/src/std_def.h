/**
 * @file std_def.h
 * @author LiYu87
 * @date 2019.09.06
 * @brief Provide C4MLIB standard macros.
 *
 */

#ifndef C4MLIB_MACRO_STD_DEF_H
#define C4MLIB_MACRO_STD_DEF_H

/* Public Section Start */
#define DISABLE 0  ///< 0 啟用 @ingroup macro_macro
#define ENABLE 1  ///< 1 關閉 @ingroup macro_macro

/**
 * @brief 確認 EN 是否為 ENABLE 或 DISABLE
 * @ingroup macro_macro
 */
#define IS_ENABLE_OR_DISABLE(EN) ((EN) == DISABLE || (EN) == ENABLE)

#define INPUT 0  ///< 0 輸入 @ingroup macro_macro
#define OUTPUT 1  ///< 1 輸出 @ingroup macro_macro

/**
 * @brief 確認 INOUT 是否為 INPUT 或 OUTPUT
 * @ingroup macro_macro
 */
#define IS_INPUT_OR_OUTPUT(INOUT) ((INOUT) == INPUT || (INOUT) == OUTPUT)
/* Public Section End */
#endif  // C4MLIB_MACRO_STD_DEF_H
