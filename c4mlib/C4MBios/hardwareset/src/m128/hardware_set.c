/**
 * @file hardware_set.c
 * @author ya058764
 * @date 2021.04.26
 * @brief
 */
#include "c4mlib/C4MBios/hardwareset/src/hardware_set.h"

#include "c4mlib/C4MBios/macro/src/bits_op.h"

#include <avr/io.h>
#include <stdint.h>
#include <stdio.h>

#define HW_SET_OK           0
#define HW_SET_FPTNUM_ERR   1
#define HW_SET_PUTNUM_ERR   2
#define HW_SET_PUTBYTES_ERR 3

uint8_t HardwareSet_step(void *void_p) {
    HardWareSet_t *str_p = (HardWareSet_t *)void_p;
    if (str_p->FlagNum != ((uint8_t *)str_p->DataList_p)[str_p->FlagNum]) {
        return HW_SET_FPTNUM_ERR;
    }
    uint8_t i = 0, j = 0, bytes_chk = 0;
    HWFlagPara_t *ptr = NULL;
    for (i = 0; i < str_p->FlagNum; i++) {
        uint8_t *data = NULL;
        ptr        = (HWFlagPara_t *)(str_p->FlagPara_p + i);
        data       = (uint8_t *)(str_p->DataList_p + i);
        REGFPT(ptr->Reg_p, ptr->Mask, ptr->Shift, *data);
    }
    HWRegPara_t *ptr2 = NULL;
    for (i = 0, j = str_p->FlagNum + 1, bytes_chk = 0; i < str_p->RegNum;
         i++, j += ptr2->Bytes, bytes_chk += ptr2->Bytes) {
        ptr2 = (HWRegPara_t *)(str_p->RegPara_p + i);
        if (ptr2->Bytes == 2) {
            uint16_t *data = NULL;
            data               = (uint16_t *)(str_p->DataList_p + j);
            uint8_t tmp[2];
            tmp[1] = HIBYTE16(*data);
            tmp[0] = LOBYTE16(*data);
            REGPUT(ptr2->Reg_p, 2, tmp);
        }
        else if (ptr2->Bytes == 1) {
            uint8_t *data = NULL;
            data       = (uint8_t *)(str_p->DataList_p + j);
            REGPUT(ptr2->Reg_p, 1, data);
        }
        else {
            return HW_SET_PUTBYTES_ERR;
        }
    }
    if (bytes_chk !=
        ((uint8_t *)str_p->DataList_p)[bytes_chk + str_p->FlagNum + 1]) {
        return HW_SET_PUTNUM_ERR;
    }
    return HW_SET_OK;
}

uint8_t HardwareInsp_step(void *void_p) {
    HardWareSet_t *str_p = (HardWareSet_t *)void_p;
    uint8_t i = 0, j = 0, bytes_chk = 0;
    HWFlagPara_t *ptr = NULL;
    for (i = 0; i < str_p->FlagNum; i++) {
        uint8_t *data = NULL;
        ptr        = (HWFlagPara_t *)(str_p->FlagPara_p + i);
        data       = (uint8_t *)(str_p->DataList_p + i);
        REGFGT(ptr->Reg_p, ptr->Mask, ptr->Shift, data);
    }
    if (i != ((uint8_t *)str_p->DataList_p)[str_p->FlagNum]) {
        return HW_SET_FPTNUM_ERR;
    }
    HWRegPara_t *ptr2 = NULL;
    for (i = 0, j = str_p->FlagNum + 1, bytes_chk = 0; i < str_p->RegNum;
         i++, j += ptr2->Bytes, bytes_chk += ptr2->Bytes) {
        ptr2 = (HWRegPara_t *)(str_p->RegPara_p + i);
        if (ptr2->Bytes == 2) {
            uint16_t *data = NULL;
            data               = (uint16_t *)(str_p->DataList_p + j);
            REGGET(ptr2->Reg_p, 2, data);
        }
        else if (ptr2->Bytes == 1) {
            uint8_t *data = NULL;
            data       = (uint8_t *)(str_p->DataList_p + j);
            REGGET(ptr2->Reg_p, 1, data);
        }
        else {
            return HW_SET_PUTBYTES_ERR;
        }
    }
    if (bytes_chk !=
        ((uint8_t *)str_p->DataList_p)[bytes_chk + str_p->FlagNum + 1]) {
        return HW_SET_PUTNUM_ERR;
    }
    return HW_SET_OK;
}
