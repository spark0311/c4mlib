/**
 * @file uart_set.h
 * @author ya058764 (ya058764@gmail.com)
 * @date 2021.05.13
 * @brief
 *
 */

#ifndef C4MLIB_HARDWARESET_M128_UART_SET_H
#define C4MLIB_HARDWARESET_M128_UART_SET_H

#include "c4mlib/C4MBios/hardwareset/src/hardware_set.h"
#include "c4mlib/C4MBios/macro/src/std_def.h"

#include <stdint.h>
/* Public Section Start */

#define UARTFLAGPARA(N)                                                        \
    {.Reg_p = &UCSR##N##C, .Mask = 0x30, .Shift = 4},                          \
    {.Reg_p = &UCSR##N##C, .Mask = 0x08, .Shift = 3},                          \
    {.Reg_p = &UCSR##N##C, .Mask = 0x06, .Shift = 1},                          \
    {.Reg_p = &UCSR##N##B, .Mask = 0x08, .Shift = 3},                          \
    {.Reg_p = &UCSR##N##B, .Mask = 0x10, .Shift = 4},                          \
    {.Reg_p = &UCSR##N##B, .Mask = 0x40, .Shift = 6},                          \
    {.Reg_p = &UCSR##N##B, .Mask = 0x80, .Shift = 7},                          \
    {.Reg_p = &UCSR##N##A, .Mask = 0x02, .Shift = 1}

#define UART0FLAGPARALISTINI                                                   \
    { {&DDRE, 0x03, 0}, UARTFLAGPARA(0) }

#define UART1FLAGPARALISTINI                                                   \
    { {&DDRD, 0x0C, 2}, UARTFLAGPARA(1) }

#define UARTREGPARALISTINI(N)                                                  \
    { {.Reg_p = &UBRR##N##L, .Bytes = 1}, {.Reg_p = &UBRR##N##H, .Bytes = 1} }

#define UART0REGPARALISTINI UARTREGPARALISTINI(0)
#define UART1REGPARALISTINI UARTREGPARALISTINI(1)

#define UARTHWSETSTRINI                                                        \
    {.FlagNum = 9, .RegNum = 2}

#define UART0HWSETSTRINI UARTHWSETSTRINI
#define UART1HWSETSTRINI UARTHWSETSTRINI

/**
 * @brief
 *@param DDn0_1         TXOUTRXIN
 *@param UMPn0_1        UART_PARITY_NONE
 *@param USBSn          UART_STOPBITS_1
 *@param UCSZn0_1       UART_WORDBITS_8
 *@param TXENn          TX ENABLE
 *@param RXENn          RX ENABLE
 *@param TXCIEn         TX INT ENABLE
 *@param RXCIEn         RX INT ENABLE
 *@param U2Xn           UART DOUBLE Baud Rate
 *@param FlagTotalBytes Total Bytes of Flag Group Datum
 *@param UBRRnL         Low Byte of UART Baud Rate
 *@param UBRRnH         High Byte of UART Baud Rate
 *@param RegTotalBytes  Total Bytes of Registers
 */
typedef struct {
    uint8_t DDn0_1;
    uint8_t UMPn0_1;
    uint8_t USBSn;
    uint8_t UCSZn0_1;
    uint8_t TXENn;
    uint8_t RXENn;
    uint8_t TXCIEn;
    uint8_t RXCIEn;
    uint8_t U2Xn;
    uint8_t FlagTotalBytes;
    uint8_t UBRRnL;
    uint8_t UBRRnH;
    uint8_t RegTotalBytes;
} UART0HWSetDataStr_t, UART1HWSetDataStr_t;

/**
 * @brief
 *
 * @param DataReg_p UART Data I/O Reg address
 * @param UARTDataBytes UART Data I/O Reg Bytes
 * @param TXEnReg_p UART TX Enable flag Reg address
 * @param TXEnMask UART TX Enable flag Mask, shift
 * @param TXEnShift
 * @param RXEnReg_p UART RX Enable flag Reg address
 * @param RXEnMask UART RX Enable flag Mask, shift
 * @param RXEnShift
 * @param TXRXSwiReg_p UART TX/RX Switch Contr Reg address
 * @param TXRXSwiMask UART TX/RX Switch Contr Mask, shift
 * @param TXRXSwiShift
 * @param TXIntEnReg_p TX Interrupt Enable flag Reg address
 * @param TXIntEnMask TX Interrupt Enable flag Mask, shift
 * @param TXIntEnShift
 * @param RXIntEnReg_p RX Interrupt Enable flag Reg address
 * @param RXIntEnMask RX Interrupt Enable flag Mask, shift
 * @param RXIntEnShift
 * @param TXIntClrReg_p TX Interrupt flag Clear Reg address
 * @param TXIntClrMask TX Interrupt flag Clear  Mask, shift
 * @param TXIntClrShift
 * @param RXIntClrReg_p RX Interrupt flag Clear Reg address
 * @param RXIntClrMask RX Interrupt flag Clear  Mask, shift
 * @param RXIntClrShift
 * @param TXIntSetReg_p TX Interrupt flag Set Reg address
 * @param TXIntSetMask TX Interrupt flag Set Mask, shift
 * @param TXIntSetShift
 * @param RXIntSetReg_p RX Interrupt flag Set Reg address
 * @param RXIntSetMask RX Interrupt flag Set Mask, shift
 * @param RXIntSetShift
 * @param TXIntFReg_p TX Interrupt Flag Register Address
 * @param TXIntFMask TX Interrupt Flag Mask and Shift
 * @param TXIntFShift
 * @param RXIntFReg_p RX Interrupt Flag Register Address
 * @param RXIntFMask RX Interrupt Flag Mask and Shift
 * @param RXIntFShift
 */
typedef struct {
    volatile uint8_t* DataReg_p;
    uint8_t UARTDataBytes;
    volatile uint8_t* TXEnReg_p;
    uint8_t TXEnMask;
    uint8_t TXEnShift;
    volatile uint8_t* RXEnReg_p;
    uint8_t RXEnMask;
    uint8_t RXEnShift;
    volatile uint8_t* TXRXSwiReg_p;
    uint8_t TXRXSwiMask;
    uint8_t TXRXSwiShift;
    volatile uint8_t* TXIntEnReg_p;
    uint8_t TXIntEnMask;
    uint8_t TXIntEnShift;
    volatile uint8_t* RXIntEnReg_p;
    uint8_t RXIntEnMask;
    uint8_t RXIntEnShift;
    volatile uint8_t* TXIntClrReg_p;
    uint8_t TXIntClrMask;
    uint8_t TXIntClrShift;
    volatile uint8_t* RXIntClrReg_p;
    uint8_t RXIntClrMask;
    uint8_t RXIntClrShift;
    volatile uint8_t* TXIntSetReg_p;
    uint8_t TXIntSetMask;
    uint8_t TXIntSetShift;
    volatile uint8_t* RXIntSetReg_p;
    uint8_t RXIntSetMask;
    uint8_t RXIntSetShift;
    volatile uint8_t* TXIntFReg_p;
    uint8_t TXIntFMask;
    uint8_t TXIntFShift;
    volatile uint8_t* RXIntFReg_p;
    uint8_t RXIntFMask;
    uint8_t RXIntFShift;
} UARTOpStr_t;

#define UARTOPSTRINI(N)                                                        \
    {                                                                          \
        .DataReg_p = &UDR##N, .UARTDataBytes = 1, .TXRXSwiReg_p = &PORTF,  \
        .TXRXSwiMask = 0x01, .TXRXSwiShift = 4, .TXEnReg_p = &UCSR##N##B,      \
        .TXEnMask = 0x10, .TXEnShift = 3, .RXEnReg_p = &UCSR##N##B,            \
        .RXEnMask = 0x40, .RXEnShift = 4, .TXIntEnReg_p = &UCSR##N##B,         \
        .TXIntEnMask = 0x40, .TXIntEnShift = 6, .RXIntEnReg_p = &UCSR##N##B,   \
        .RXIntEnMask = 0x80, .RXIntEnShift = 7, .TXIntClrReg_p = &UCSR##N##A,  \
        .TXIntClrMask = 0x40, .TXIntClrShift = 6,                              \
        .RXIntClrReg_p = &UCSR##N##A, .RXIntClrMask = 0x80,                    \
        .RXIntClrShift = 7, .TXIntSetReg_p = &UCSR##N##A,                      \
        .TXIntSetMask = 0x40, .TXIntSetShift = 6,                              \
        .RXIntSetReg_p = &UCSR##N##A, .RXIntSetMask = 0x80,                    \
        .RXIntSetShift = 7, .TXIntFReg_p = &UCSR##N##A, .TXIntFMask = 0X20,    \
        .TXIntFShift = 5, .RXIntFReg_p = &UCSR##N##A, .RXIntFMask = 0x80,      \
        .RXIntFShift = 7                                                       \
    }

#define UART0OPSTRINI UARTOPSTRINI(0)
#define UART1OPSTRINI UARTOPSTRINI(1)
/* Public Section End */
#endif  // C4MLIB_HARDWARESET_M128_UART_SET_H
