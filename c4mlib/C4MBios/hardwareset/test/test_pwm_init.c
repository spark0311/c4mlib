/**
 * @file test_pwm_init.c
 * @author ya058764 (ya058764@gmail.com)
 * @date 2021.05.15
 * @brief 
 * 
 */

#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/C4MBios/hardwareset/src/m128/pwm_set.h"
#include "c4mlib/config/pwm.cfg"
#include <stdio.h>

// HardWare Mask and Shift
#define HWMS(a, b, c) (a & b) >> c

uint8_t PWM0_init(void){
    HardWareSet_t PWM0HWSet_str = PWM0HWSETSTRINI;
    HWFlagPara_t PWM0FgGpData_str[6] = PWM0FLAGPARALISTINI;
    HWRegPara_t PWM0RegData_str = PWM0REGPARALISTINI;
    PWM0HWSetDataStr_t PWM0HWSetData = PWM0SETDATALISTINI;
    HARDWARESET_LAY(PWM0HWSet_str, PWM0FgGpData_str[0], PWM0RegData_str, PWM0HWSetData);
    return HardwareSet_step(&PWM0HWSet_str);
}

uint8_t PWM1_init(void){
    HardWareSet_t PWM1HWSet_str = PWM1HWSETSTRINI;
    HWFlagPara_t PWM1FgGpData_str[10] = PWM1FLAGPARALISTINI;
    HWRegPara_t PWM1RegData_str[4] = PWM1REGPARALISTINI;
    PWM1HWSetDataStr_t PWM1HWSetData = PWM1SETDATALISTINI;
    HARDWARESET_LAY(PWM1HWSet_str, PWM1FgGpData_str[0], PWM1RegData_str[0], PWM1HWSetData);
    return HardwareSet_step(&PWM1HWSet_str);
}

uint8_t PWM2_init(void){
    HardWareSet_t PWM2HWSet_str = PWM2HWSETSTRINI;
    HWFlagPara_t PWM2FgGpData_str[5] = PWM2FLAGPARALISTINI;
    HWRegPara_t PWM2RegData_str = PWM2REGPARALISTINI;
    PWM2HWSetDataStr_t PWM2HWSetData = PWM2SETDATALISTINI;
    HARDWARESET_LAY(PWM2HWSet_str, PWM2FgGpData_str[0], PWM2RegData_str, PWM2HWSetData);
    return HardwareSet_step(&PWM2HWSet_str);
}

uint8_t PWM3_init(void){
    HardWareSet_t PWM3HWSet_str = PWM3HWSETSTRINI;
    HWFlagPara_t PWM3FgGpData_str[10] = PWM3FLAGPARALISTINI;
    HWRegPara_t PWM3RegData_str[4] = PWM3REGPARALISTINI;
    PWM3HWSetDataStr_t PWM3HWSetData = PWM3SETDATALISTINI;
    HARDWARESET_LAY(PWM3HWSet_str, PWM3FgGpData_str[0], PWM3RegData_str[0], PWM3HWSetData);
    return HardwareSet_step(&PWM3HWSet_str);
}

int main() {
    C4M_DEVICE_set();

    // PWM0:
    printf("PWM0 init = %d\n", PWM0_init());
    printf("ASn = %d\n", HWMS(ASSR, 0x08, 3));
    printf("WGMn0_1 = %d\n", HWMS(TCCR0, 0x48, 3));
    printf("CSn0_2 = %d\n", HWMS(TCCR0, 0x07, 0));
    printf("COMn0_1 = %d\n", HWMS(TCCR0, 0x30, 4));
    printf("DDx = %d\n", HWMS(DDRB, 0x10, 4));
    printf("OCRn = %d\n", OCR0);
    printf("\n");

    // PWM1:
    printf("PWM1 init = %d\n", PWM1_init());
    printf("WGMn0_1 = %d\n", HWMS(TCCR1A, 0x03, 0));
    printf("WGMn2_3 = %d\n", HWMS(TCCR1B, 0x18, 3));
    printf("CSn0_2 = %d\n", HWMS(TCCR1B, 0x07, 0));
    printf("COMnA0_1 = %d\n", HWMS(TCCR1A, 0xc0, 6));
    printf("COMnB0_1 = %d\n", HWMS(TCCR1A, 0x30, 4));
    printf("COMnC0_1 = %d\n", HWMS(TCCR1A, 0x0c, 2));
    printf("DDxA = %d\n", HWMS(DDRB, 0xe0, 5));
    printf("DDxB = %d\n", HWMS(DDRB, 0x40, 6));
    printf("DDxC = %d\n", HWMS(DDRB, 0x80, 7));
    printf("ICRn = %d\n", ICR1L);
    printf("OCRnA = %d\n", OCR1AL);
    printf("OCRnB = %d\n", OCR1BL);
    printf("OCRnC = %d\n", OCR1CL);
    printf("\n");

    // PWM2:
    printf("PWM2 init = %d\n", PWM2_init());
    printf("WGMn0_1 = %d\n", HWMS(TCCR2, 0x48, 3));
    printf("CSn0_2 = %d\n", HWMS(TCCR2, 0x07, 0));
    printf("COMn0_1 = %d\n", HWMS(TCCR2, 0x30, 4));
    printf("DDx = %d\n", HWMS(DDRB, 0x80, 7));
    printf("OCRn = %d\n", OCR0);
    printf("\n");

    // PWM3:
    printf("PWM3 init = %d\n", PWM3_init());
    printf("WGMn0_1 = %d\n", HWMS(TCCR3A, 0x03, 0));
    printf("WGMn2_3 = %d\n", HWMS(TCCR3B, 0x18, 3));
    printf("CSn0_2 = %d\n", HWMS(TCCR3B, 0x07, 0));
    printf("COMnA0_1 = %d\n", HWMS(TCCR3A, 0xc0, 6));
    printf("COMnB0_1 = %d\n", HWMS(TCCR3A, 0x30, 4));
    printf("COMnC0_1 = %d\n", HWMS(TCCR3A, 0x0c, 2));
    printf("DDxA = %d\n", HWMS(DDRB, 0x08, 3));
    printf("DDxB = %d\n", HWMS(DDRB, 0x10, 4));
    printf("DDxC = %d\n", HWMS(DDRB, 0x20, 5));
    printf("ICRn = %d\n", ICR3L);
    printf("OCRnA = %d\n", OCR3AL);
    printf("OCRnB = %d\n", OCR3BL);
    printf("OCRnC = %d\n", OCR3CL);
}
