/**
 * @file test_ext4_init.c
 * @author ya058764 (ya058764@gmail.com)
 * @date 2021.05.14
 * @brief 
 * 
 */

#include "c4mlib/C4MBios/hardwareset/src/m128/ext_set.h"
#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/config/ext.cfg"
#include <avr/interrupt.h>
int i=0;


uint8_t EXT4_init(void){
    HardWareSet_t EXT4HWSet_str = EXTHWSETSTRINI;
    HWFlagPara_t EXT4FgGpData_str[3] = EXT4FLAGPARALISTINI;
    HWRegPara_t EXT4RegData_str = EXT4REGPARALISTINI;
    EXTHWsetDataStr_t EXT4HWSetData = EXT4SETDATALISTINI;
    HARDWARESET_LAY(EXT4HWSet_str, EXT4FgGpData_str[0], EXT4RegData_str, EXT4HWSetData);
    return HardwareSet_step(&EXT4HWSet_str);
}
int main(){
    C4M_DEVICE_set();
    
    uint8_t res = EXT4_init();
    printf("hardware_set report %d \n", res);
    printf("DDRE = %d\n", ((DDRE & 0x10) >> 4));
    printf("EICRB = %d\n", ((EICRB & 0x03) >> 0));
    printf("EIMSK = %d\n", ((EIMSK & 0x10) >> 4));
    sei();
    while (1);    
}

ISR(INT4_vect)
{
   i++;
   printf("ext complete, i=%d\n", i);
}
