/**
 * @file test_twi_init.c
 * @author ya058764 (ya058764@gmail.com)
 * @date 2021.05.15
 * @brief 
 * 
 */

#include "c4mlib/C4MBios/device/src/device.h"
#include "c4mlib/C4MBios/hardwareset/src/m128/twi_set.h"
#include "c4mlib/config/twi.cfg"

uint8_t TWI_init(void){
    HardWareSet_t TWIHWSet_str = TWIHWSETSTRINI;
    HWFlagPara_t TWIFgGpData_str[6] = TWIFLAGPARALISTINI;
    HWRegPara_t TWIRegData_str = TWIREGPARALISTINI;
    TWIHWSetDataStr_t TWIHWSetData = TWISETDATALISTINI;
    HARDWARESET_LAY(TWIHWSet_str, TWIFgGpData_str[0], TWIRegData_str, TWIHWSetData);
    return HardwareSet_step(&TWIHWSet_str);
}

int main() {
    C4M_DEVICE_set();
    uint8_t res = TWI_init();
    printf("hardware_set report %d \n", res);
    printf("TWBR:%d\n", TWBR);
    printf("TWCR:%d\n", TWCR);
    printf("TWAR:%d\n", TWAR);
    printf("DDRD:%d\n", DDRD);
}
