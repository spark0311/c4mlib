/**
 * @file test_spi_init.c
 * @author ya058764 (ya058764@gmail.com)
 * @date 2021.05.15
 * @brief 
 * 
 */

#include "c4mlib/config/spi.cfg"
#include "c4mlib/C4MBios/hardwareset/src/m128/spi_set.h"
#include "c4mlib/C4MBios/device/src/device.h"

uint8_t SPI_init(void){
    HardWareSet_t SPIHWSet_str = SPIHWSETSTRINI;
    HWFlagPara_t SPIFgGpData_str[11] = SPIFLAGPARALISTINI;
    HWRegPara_t SPIRegData_str = SPIREGPARALISTINI;
    SPIHWSetDataStr_t SPIHWSetData = SPISETDATALISTINI;
    HARDWARESET_LAY(SPIHWSet_str, SPIFgGpData_str[0], SPIRegData_str, SPIHWSetData);
    return HardwareSet_step(&SPIHWSet_str);
}

int main(){
    C4M_DEVICE_set();
    
    uint8_t res = SPI_init();
    if (res)
    {
        printf("init fail: %d\n",res);
        return 0;
    }
    printf("hardware_set report %d \n", res);
    printf("SPI2SPEED = %d\n", ((SPSR & 0x01) >> 0));
    printf("SPR0_1    = %d\n", ((SPCR & 0x03) >> 0));
    printf("CPHASE    = %d\n", ((SPCR & 0x04) >> 2));
    printf("CPOLAR    = %d\n", ((SPCR & 0x08) >> 3));
    printf("MSSELECT  = %d\n", ((SPCR & 0x10) >> 4));
    printf("DORDER    = %d\n", ((SPCR & 0x20) >> 5));
    printf("DDx0_3    = %d\n", ((DDRB & 0x0F) >> 0));
    printf("SPE       = %d\n", ((SPCR & 0x40) >> 6));
    printf("SPIE      = %d\n", ((SPCR & 0x80) >> 7));
}
